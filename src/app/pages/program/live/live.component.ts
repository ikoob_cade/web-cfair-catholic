import * as _ from 'lodash';
import { Component, OnInit, OnDestroy, HostListener, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { forkJoin as observableForkJoin, Observable } from 'rxjs';
import { AgendaService } from 'src/app/services/api/agenda.service';
import { DateService } from 'src/app/services/api/date.service';
import { RoomService } from 'src/app/services/api/room.service';
import { map } from 'rxjs/operators';
import { SocketService } from 'src/app/services/socket/socket.service';

@Component({
  selector: 'app-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LiveComponent implements OnInit, OnDestroy, AfterViewInit {
  public user: any; // 사용자 정보
  public live: any; // 라이브 방송
  public isLiveAgenda: any; // 현재 진행중인 아젠다
  public liveAgendaState: any;

  public attendance: boolean; // 출석상태

  public dates: any[] = []; // 날짜 목록
  public rooms: any[] = []; // 룸 목록

  public allAgendas: any[] = []; // 모든 강의
  public agendas: any[] = []; // 날짜/룸으로 필터링 된 강의.

  public selected: any = {
    date: null,
    room: null
  };

  public showPlayer = false;

  public banners = []; // 광고배너 리스트
  public bannersLiveChat = []; // 플레이어에 넘겨줄 채팅창 위 롤링배너 데이타

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    private dateService: DateService,
    private roomService: RoomService,
    private agendaService: AgendaService,
    private socketService: SocketService,
  ) {
  }

  ngOnInit(): void {
    this.doInit();
  }

  ngAfterViewInit(): void {
  }

  doInit(): void {
    this.user = JSON.parse(localStorage.getItem('cfair'));
    if (!this.user || this.user.eventId !== '6004e969c25b63001133f993') {
      localStorage.removeItem('cfair');
      this.router.navigate([`/login`]);
      return null;
    }

    const observables = [this.getDates(), this.getRooms()];
    observableForkJoin(observables)
      .pipe(
        map(resp => {
          this.dates = resp[0];
          this.rooms = resp[1];
          return resp;
        })
      )
      .subscribe(res => {
        let date = this.dates[0];
        let room = this.rooms[0];
        this.route.queryParams.subscribe(param => {
          // 선택된 날짜/룸이 있다면 해당 날짜와 룸이 선택될 수 있도록 한다.
          if (param.dateId && param.roomId) {
            date = this.dates.find(date => {
              return date.id === param.dateId;
            });
            room = this.rooms.find(room => {
              return room.id === param.roomId;
            });
          }
          this.router.navigate([], { queryParams: { dateId: null, roomId: null }, queryParamsHandling: 'merge' });
        });
        this.selected = { date: date, room: room };
        this.setAgendaList(date, room);
      });
  }

  public next: any;
  // Date/Room 선택 시 Agenda Filtering
  setAgendaList(date: any, room: any, next?: boolean): any {
    /**
     * 날짜의 옵션값 확인해서 player컴포넌트 출력을 제어한다.
     * 선택한 날짜의 ON/OFF 상태를 확인해서 플레이어, 채팅 이용 불가하도록 하기 위함
     */
    if (!date.showPlayer) {
      this.showPlayer = false;
    } else {
      this.showPlayer = true;
    }

    // 소켓 접속
    setTimeout(() => {
      this.socketService.leave(this.selected.room.id);
      this.socketService.join(room.id);
    }, this.user ? 0 : 300);
    // 입장/퇴장
    if (next) {
      this.next = null;
    }
    setTimeout(() => {
      this.selected = {
        date,
        room
      };

      this.getAgendasV2(date.id, room.id).subscribe(agendas => {
        this.agendas = agendas;
        setTimeout(() => {
          this.live = this.selected.room.contents;
        }, 100);
      });
      this.live = null; // new app-player
    }, 500);
  }

  /* 아젠다 목록 조회 V2
   * date, room 값을 파라미터에 추가했음.
   * 위 조건에 해당하는 아젠다만 받아온다 => 서버 부하 감소
   */
  getAgendasV2(dateId, roomId): Observable<any> {
    return this.agendaService.findV2(dateId, roomId);
  }

  /** 날짜 목록 조회 */
  getDates(): Observable<any> {
    return this.dateService.find();
  }

  /** 룸 목록 조회 */
  getRooms(): Observable<any> {
    return this.roomService.find();
  }

  /**
   * 강의 상세보기
   * @param agenda 강의
   */
  goDetail(agenda: any): void {
    this.router.navigate([`/live/${this.selected.date.id}/${this.selected.room.id}/${agenda.id}`]);
  }

  /**
   * 페이지 나갈 때 처리
   */
  ngOnDestroy(): void {
    if (this.user) {
      this.socketService.leave(this.selected.room.id);
    }
  }

  /**
   * 이 페이지 벗어나기 전에(window:beforeunload) history('out')을 위한 이벤트 리스너
   * @param $event 이벤트
   */
  @HostListener('window:beforeunload', ['$event'])
  public beforeunloadHandler($event): boolean {

    $event.preventDefault();
    // tslint:disable-next-line: deprecation
    ($event || window.event).returnValue = '로그아웃 하시겠습니까?';
    return false;
  }
}
