import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit, AfterViewInit {
  @ViewChild('policyAlertBtn') policyAlertBtn: ElementRef;
  private user: any;
  public passwordError = ''; // 로그인 에러메세지
  public member = {
    email: '',
    password: '',
  };

  constructor(
    public router: Router,
    public auth: AuthService) { }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    this.policyAlertBtn.nativeElement.click();
  }

  login(): void {
    if (this.loginValidator()) {
      this.auth.login(this.member).subscribe(res => {
        if (res.token) {
          this.user = res;
          this.passwordError = '';

          this.goToMain();
        }
      }, error => {
        if (error.status === 401) {
          this.passwordError = '계정정보가 잘못되었습니다.';
        }
        if (error.status === 404) {
          this.passwordError = '아이디를 찾을 수 없습니다.';
        }
      });
    } else {
      alert('값을 모두 입력해주세요.');
    }
  }

  loginValidator(): boolean {
    if (!this.member.email || !this.member.password) {
      return false;
    }
    return true;
  }

  /**
   * 로그인 정보를 스토리지 저장 및 메인으로 이동.
   */
  goToMain(): void {
    localStorage.setItem('cfair', JSON.stringify(this.user));
    this.router.navigate(['/main']);
  }
}
