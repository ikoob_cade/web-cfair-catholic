import * as _ from 'lodash';
import { _ParseAST } from '@angular/compiler';
import { Component, OnInit, AfterViewInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  @ViewChild('policyAlertBtn') policyAlertBtn: ElementRef;
  public sponsors: Array<any> = []; // 스폰서 목록
  public selectedSponsor: any;
  public mobile: boolean;

  public user;

  public attachments = []; // 스폰서 첨부파일

  constructor(
  ) { }


  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    if (window.innerWidth < 768 !== this.mobile) {
    }
  }

  ngOnInit(): void {
    this.user = localStorage.getItem('cfair');
    // this.loadSponsors();
  }
}
