import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { MemberService } from 'src/app/services/api/member.service';
import { SocketService } from 'src/app/services/socket/socket.service';

declare var $: any;
enum Change_Password_Message {
  FILL_ALL = '모든 항목을 입력해 주세요.',
  FAILED_NEW_CONFIRM = '새 비밀번호 확인이 잘못 입력되었습니다.\n다시 확인해 주세요.',
  SUCCESS = '비밀번호가 변경되었습니다.'
}

@Component({
  selector: 'app-my-page',
  templateUrl: './my-page.component.html',
  styleUrls: ['./my-page.component.scss']
})
export class MyPageComponent implements OnInit {
  public user: any;

  constructor(
    public router: Router,
    private authService: AuthService,
    private memberService: MemberService,
    private socketService: SocketService,
  ) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('cfair'));
    this.getMyInfo();
  }

  /** 사용자 정보 조회 */
  getMyInfo(): void {
    this.memberService.getMyInfo(this.user.id).subscribe(res => {
      // console.log('GET MyInfo', res);
      if (res) {
        this.user = res;
      }
    });
  }

  logout(): void {
    this.authService.logout().subscribe(res => {
      this.logoutSocket();

      localStorage.removeItem('cfair');
      this.router.navigate(['/login']);
    }, error => {
      localStorage.removeItem('cfair');
      this.router.navigate(['/login']);
    });
  }

  logoutSocket(): void {
    this.socketService.logout({
      memberId: this.user.id
    });
  }

}
