import { DeviceDetectorService } from 'ngx-device-detector';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule, COMPOSITION_BUFFER_MODE } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { MomentModule } from 'ngx-moment';
import { SocketService } from './services/socket/socket.service';
import { NgImageSliderModule } from 'ng-image-slider';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { CookieService } from 'ngx-cookie-service';

// Service
import { ApiService } from './services/api/api.service';
import { AuthService } from './services/auth/auth.service';
import { AuthInterceptorService } from './services/auth-interceptor/auth-interceptor.service';
import { AgendaService } from './services/api/agenda.service';
import { EventService } from './services/api/event.service';
import { DateService } from './services/api/date.service';
import { RoomService } from './services/api/room.service';
import { MemberService } from './services/api/member.service';
import { HistoryService } from './services/api/history.service';

// Pages
import { AppComponent } from './app.component';
import { MainComponent } from './pages/main/main.component';
import { LoginComponent } from './pages/login/login.component';
import { BannerComponent } from './components/banner/banner.component';
import { MyPageComponent } from './pages/my-page/my-page.component';

import { AgendaComponent } from './components/agenda/agenda.component';

import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';

// Components
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { PlayerComponent } from './components/player/player.component';
import { PlayerUstreamComponent } from './components/player-ustream/player-ustream.component';
import { LiveComponent } from './pages/program/live/live.component';
import { SafeHtmlPipe } from './pipes/safe-html/safe-html.pipe';
import { NotSupportIeComponent } from './pages/not-support-ie/not-support-ie.component';
import { SessionComponent } from './components/session/session.component';

const pages = [
  MainComponent,
  LoginComponent,
  PageNotFoundComponent,
  LiveComponent,
  MyPageComponent,
]

const components = [
  HeaderComponent,
  FooterComponent,
  BannerComponent,
  PlayerComponent,
  PlayerUstreamComponent,
  AgendaComponent,
  NotSupportIeComponent,
  SessionComponent,
]

const pipes = [
  SafeHtmlPipe,
]

@NgModule({
  declarations: [
    AppComponent,
    ...pages,
    ...components,
    ...pipes,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MomentModule,
    NgImageSliderModule,
    PdfViewerModule,
  ],
  providers: [
    ApiService,
    AuthService,
    DatePipe,
    AgendaService,
    EventService,
    DateService,
    RoomService,
    MemberService,
    DeviceDetectorService,
    SocketService,
    CookieService,
    HistoryService,
    PdfViewerModule,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    },
    { provide: COMPOSITION_BUFFER_MODE, useValue: false },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
