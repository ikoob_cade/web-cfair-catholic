import { Component, OnInit, AfterViewInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { _ParseAST } from '@angular/compiler';
// import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import '@devmobiliza/videojs-vimeo/dist/videojs-vimeo.esm';
import { MemberService } from 'src/app/services/api/member.service';
import * as _ from 'lodash';

declare let UstreamEmbed: any;

/**
 * TODO
 * IBM Ustream 라이브시 이 컴포넌트를 사용한다.
 * 삼성서울병원 전용 air에서 개발 되어서. Room이 하나밖에 없었기 때문에 live url이 하드코딩 되어있다.
 * 각 룸에 liveUrl을 등록 해서 url 추적하는 방식으로 개발 필요하다.
 */
@Component({
  selector: 'app-player-ustream',
  templateUrl: './player-ustream.component.html',
  styleUrls: ['./player-ustream.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PlayerUstreamComponent implements OnInit, AfterViewInit {
  public user: any;
  public viewer: any;

  constructor(
    public fb: FormBuilder,
    private cdr: ChangeDetectorRef,
  ) {
  }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('cfair'));
    this.setUstreamViewer();
  }

  ngAfterViewInit(): void {
    this.cdr.detectChanges();
  }

  setUstreamViewer(): void {
    if (UstreamEmbed) {
      this.viewer = UstreamEmbed('UstreamIframe');
      // document.getElementById('UstreamIframe').style.minHeight = '-webkit-fill-available';

      this.viewer.addListener('live', event => {
        console.log('event = ', event);
      });

      this.viewer.addListener('finished', event => {
        console.log('event = ', event);
        this.viewer.getProperty('progress', progress => {
          console.log('progress = ' + progress);
        });
      });

      this.viewer.addListener('offline', event => {
        console.log('event = ', event);
        this.viewer.getProperty('progress', progress => {
          console.log('progress = ', progress);
        });
      });

      this.viewer.addListener('playing', event => {
        console.log('event = ', event);
      });

    } else {
      console.log('no UstreamEmbed');
    }
  }
}
