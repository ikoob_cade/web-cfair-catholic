import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.scss']
})
export class AgendaComponent implements OnInit, OnDestroy {
  @Input('agenda') agenda: any; // 아젠다 정보
  @Output('detailFn') detailFn = new EventEmitter(); // 자세히보기

  public isLive: boolean = false;
  public polling: any;
  public title: Array<string> = [];

  constructor(
    private router: Router,
  ) { }

  ngOnInit(): void {
    if (this.agenda.title.includes('-')) {
      let splitStr = this.agenda.title.split('-');
      for (let str of splitStr) {
        this.title.push(str);
      }
    } else {
      this.title.push(this.agenda.title);
    }
  }


  ngOnDestroy() {
    clearTimeout(this.polling);
  }

}
