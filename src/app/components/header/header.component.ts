import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { MemberService } from 'src/app/services/api/member.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { SocketService } from 'src/app/services/socket/socket.service';

declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public user: any;
  collapse = true;

  constructor(
    private router: Router,
    private authService: AuthService,
    private memberService: MemberService,
    private socketService: SocketService,
  ) {
    router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((val) => {
        const auth = JSON.parse(localStorage.getItem('cfair'));
        this.user = (auth && auth.token) ? auth : undefined;

        if (this.user) {
          this.loginSocket();
        }

        $('.navbar-collapse').collapse('hide');
        this.collapse = true;
      });
  }

  ngOnInit(): void {
  }

  /** 로그아웃 */
  logout(): void {
    this.authService.logout().subscribe(res => {
      if (window.confirm('로그아웃 하시겠습니까?')) {
        this.logoutSocket();
        localStorage.removeItem('cfair');
        this.router.navigate(['/login']);
      }
    }, error => {
      localStorage.removeItem('cfair');
      this.router.navigate(['/']);
    });
  }

  loginSocket() {
    this.socketService.login({
      memberId: this.user.id,
      token: this.user.token
    });
  }

  logoutSocket() {
    this.socketService.logout({
      memberId: this.user.id
    })
  }


}
