// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // base_url: 'http://d2v.ikoob.com:8038/api/v1', // 개발서버
  // base_url: 'http://localhost:8038/api/v1', // 로컬
  base_url: 'https://cfair-api.cf-air.com/api/v1', // 운영서버
  eventId: '6004e969c25b63001133f993' // atw
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
